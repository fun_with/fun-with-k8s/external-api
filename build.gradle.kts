import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.6.4"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.10"
	kotlin("plugin.spring") version "1.6.10"
	// Analyze coverage
	jacoco
	// Quality control
	id("org.sonarqube") version "3.3"
	// To be able to download licenses
	id("com.github.hierynomus.license") version "0.16.1"
	// To build Docker image
	id("com.google.cloud.tools.jib") version "3.2.0"
}

group = "io.yodamad.fun_with"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}


// Test coverage
val jacocoXmlReport = "${buildDir}/reports/jacoco/xml/jacoco.xml"
tasks.test {
	finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}
tasks.jacocoTestReport {
	dependsOn(tasks.test) // tests are required to run before generating the report
	reports {
		// Enable CSV for badge on project
		csv.required.set(true)
		csv.outputLocation.set(file("${buildDir}/reports/jacoco/csv/jacoco.csv"))

		// Enable XML to convert to cobertura format for MR display -> TODO
		xml.required.set(true)
		xml.outputLocation.set(file(jacocoXmlReport))

		// Enable HTML to publish on Pages -> TODO
		html.outputLocation.set(file("${buildDir}/reports/jacoco/html"))
	}
}

// Sonarqube analysis
sonarqube {
	properties{
		property("sonar.host.url", "https://sonarcloud.io")
		property("sonar.tests", "${project.projectDir}/src/test/kotlin/")
		property("sonar.java.coveragePlugin", "jacoco")
		property("sonar.coverage.jacoco.xmlReportPaths", jacocoXmlReport)
	}
}

// Need for licence download
configurations {
	// Duplicate for other key words if need, such as compileOnly
	implementation.configure {
		isCanBeResolved = true
	}
}

// Download licenses for license checks
downloadLicenses {
	includeProjectDependencies = true
	dependencyConfiguration = "implementation"
}
license {
	ignoreFailures = true
}