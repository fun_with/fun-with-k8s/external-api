package io.yodamad.fun_with

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BuildpackApplication

fun main(args: Array<String>) {
	runApplication<BuildpackApplication>(*args)
}
