package io.yodamad.fun_with.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * API for a simple action
 */
@RestController
@RequestMapping("/api")
class HelloApi {

    @GetMapping("/hello")
    fun sayHello() = "👋 Hello wonderful 🌍 ! 🚀 🦊"
}